const Bombparty = {

	inputId: 'WordInputBox',
	endSymbol: '$',
	words: [],

	Start: function () {
		const eInput = Bombparty.GetInput();
		if (eInput) {
			console.log('Bombparty Cheat Activated !');
			eInput.oninput = e => Bombparty.OnInput(e);
			// eInput.onchange = e => Bombparty.OnChange(e);
			Bombparty.Shuffle();
		}
	},

	Shuffle: function() {
    	for (let i = 0; i < Dictionary.length; i++) {
        	const j = Math.floor(Math.random() * (i + 1));
        	[Dictionary[i], Dictionary[j]] = [Dictionary[j], Dictionary[i]];
    	}
	},

	GetInput: function () {
		return document.querySelector('#' + Bombparty.inputId);
	},

	OnInput: function (e) {
		const eInput = e.target;
		// If type '$', start the autocomplete
		if (eInput.value.includes(Bombparty.endSymbol)) {
			const sub = eInput.value.slice(0, -1).toLowerCase();
			console.log('Sub : "' + sub + '"');
			// Check if one of word contain the sub
			for (let word of Dictionary) {
				if (word.includes(sub) && !Bombparty.words.includes(word)) {
					// Replace value with the complete word
					console.log('Word : "' + word + '"');
					eInput.value = word;
					// const eForm = Bombparty.SubmitForm();
					Bombparty.words.push(word);
					console.log('Words : ' + Bombparty.words);
					break;
				}
			}
		}
	}
};

Bombparty.Start();